import { Component } from '@angular/core';
import { ServiceService } from './service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  activeUsers = ['Max', 'Anna'];
  inactiveUsers = ['Chris', 'Manu'];
users:any;
  constructor(private ser:ServiceService) {

this.ser.getAll().then(res=>{
    this.users=res["user"];     

    });

  }

  onSetToInactive(data: Object) {
 

    
    this.users.map((data2) => {
     
        if (data["name"] == data2.name) {
          data2.activo = 0;
       
        }
        return data2;
    
    });
  }

  onSetToActive(data: object) {

    this.users.map((data2) => {
     
      if (data["name"] == data2.name) {
        data2.activo = 1;
     
      }
      return data2;
  
  });
  }


}
