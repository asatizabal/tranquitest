import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class ServiceService {

  constructor(private http: Http) {
  }


  getAll(){
    return new Promise(resolve=>{
      let response = this.http.get("assets/user.json").map(res => res.json());
      response.subscribe(data=>{
        resolve(data);
      });
    }); 
  }


}
